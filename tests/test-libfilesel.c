/*
 * test-libfilesel.c: a small test program for libgnomefilesel
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 *
 */

#include <config.h>

#include "gnome-file-selector-util.h"

#include <bonobo/bonobo-main.h>

#include <gtk/gtkmain.h>

#include <libgnomeui/gnome-init.h>

#include <liboaf/oaf-mainloop.h>

static gint
get_files (gpointer data)
{
	char *s, **strv;
	int i;

	s = gnome_file_selector_open (NULL, FALSE, NULL, NULL, "/etc");
	g_print ("open test:\n\t%s\n", s);
	g_free (s);

	s = gnome_file_selector_save (NULL, FALSE, NULL, NULL, "/tmp", NULL);
	g_print ("save test:\n\t%s\n", s);
	g_free (s);

	strv = gnome_file_selector_open_multi (NULL, TRUE, NULL, NULL, NULL);
	g_print ("open multi test:\n");
	if (strv) {
		for (i = 0; strv[i]; i++)
			g_print ("\t%s\n", strv[i]);

		g_strfreev (strv);
	}

	gtk_main_quit ();

	return FALSE;
}

int
main (int argc, char *argv[])
{
	gnome_init ("test-libgnomefilesel", "0.1", argc, argv);
	bonobo_init (oaf_init (argc, argv), NULL, NULL);

	g_idle_add (get_files, NULL);
	bonobo_main ();

	return 0;
}

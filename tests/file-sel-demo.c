/*
 * file-sel-demo.c: demonstrates how to use the file-sel
 *
 * Authors:
 *    Jacob Berkman  <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include "gnome-file-selector-util.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <bonobo/bonobo-main.h>

#include <gtk/gtkbutton.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksignal.h>
#include <gtk/gtktable.h>
#include <gtk/gtktext.h>
#include <gtk/gtkwindow.h>

#include <libgnome/gnome-defs.h>

#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-init.h>

#include <liboaf/oaf-mainloop.h>

static GtkWidget *toplevel;

static void
load_file (GtkWidget *button, GtkWidget *text)
{
	char *file, *s;
	char buf[BUFSIZ];
	int pos, fd, len;

	file = gnome_file_selector_open (GTK_WINDOW (toplevel),
					 FALSE, NULL, NULL, NULL);

	if (!file)
		return;

	gtk_text_freeze (GTK_TEXT (text));
	gtk_editable_delete_text (GTK_EDITABLE (text), 0, -1);
	pos = 0;

	fd = open (file, O_RDONLY);
	if (fd == -1) {
		s = g_strdup_printf ("There was an error loading \"%s\": %s\n",
				     file, g_strerror (errno));
		gtk_editable_insert_text (GTK_EDITABLE (text),
					  s, strlen (s), &pos);
		g_free (s);

		goto we_are_done;
	}
	
	/* this is not a gnome-vfs async demo */
	while ( 0 < (len = read (fd, buf, BUFSIZ))) {
		gtk_editable_insert_text (GTK_EDITABLE (text),
					  buf, len, &pos);
	}

 we_are_done:
	gtk_text_thaw (GTK_TEXT (text));
	g_free (file);
}

static void
create_main_window (void)
{
	GtkWidget *table;
	GtkWidget *text;
	GtkWidget *button;
	GtkWidget *scroll;

	toplevel = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (toplevel), 
			      "Lame File Selector Demo");
	gtk_widget_set_usize (toplevel, 400, 300);

	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_container_set_border_width (GTK_CONTAINER (table), 6);
	gtk_container_add (GTK_CONTAINER (toplevel), table);

	scroll = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
					GTK_POLICY_NEVER,
					GTK_POLICY_ALWAYS);
	gtk_table_attach (GTK_TABLE (table), scroll,
			  0, 2,
			  0, 1,
			  GTK_EXPAND | GTK_FILL,
			  GTK_EXPAND | GTK_FILL,
			  0, 0);
	
	text = gtk_text_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (scroll), text);

	button = gtk_button_new_with_label ("Load File");
	gtk_table_attach (GTK_TABLE (table), button,
			  0, 1,
			  1, 2,
			  GTK_FILL,
			  0,
			  0, 0);

	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (load_file),
			    text);

	button = gtk_button_new_with_label ("Quit");
	gtk_table_attach (GTK_TABLE (table), button,
			  1, 2,
			  1, 2,
			  GTK_FILL,
			  0, 
			  0, 0);

	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (gtk_main_quit),
			    NULL);

	gtk_signal_connect (GTK_OBJECT (toplevel), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_main_quit),
			    NULL);

	gtk_widget_show_all (toplevel);
}

int 
main (int argc, char *argv[])
{
	gnome_init ("file-sel-demo", "0.2", argc, argv);
	bonobo_init (oaf_init (argc, argv), NULL, NULL);

	create_main_window ();

	bonobo_main ();

	return 0;
}
